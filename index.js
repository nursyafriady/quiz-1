// Soal 1
function jumlah_kata(string) {
  var hapusSpasi = string.trim();
  var stringKata = hapusSpasi.split(" ");
  var totalKata = stringKata.length;
  return totalKata;
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = "Saya Iqbal";

jumlah_kata(kalimat_1); // 6
jumlah_kata(kalimat_2); // 2

console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));

// Soal 2
function next_date(bulan) {
  switch (bulan) {
    case 1: {
      var nextBulan = " Februari ";
      break;
    }
    case 2:
      var nextBulan = " Maret ";
      break;
    case 3:
      var nextBulan = " April ";
      break;
    case 4:
      var nextBulan = " Mei ";
      break;
    case 5:
      var nextBulan = " Juni ";
      break;
    case 6:
      var nextBulan = " Juli ";
      break;
    case 7:
      var nextBulan = " Agustus ";
      break;
    case 8:
      var nextBulan = " September ";
      break;
    case 9:
      var nextBulan = " Oktober ";
      break;
    case 10:
      var nextBulan = " November ";
      break;
    case 11:
      var nextBulan = " Desember ";
      break;
    case 12:
      var nextBulan = " Januari ";
      break;
    default:
      console.log("Tanggal Lahir Tidak Ditemukan");
  }

  return nextBulan;
}

var bulan = 2;

console.log(next_date(bulan));
